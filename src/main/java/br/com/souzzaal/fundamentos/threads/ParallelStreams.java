package br.com.souzzaal.fundamentos.threads;

import java.util.stream.IntStream;

/**
 * Exemplo de uso do ParallelStream, recurso do Java 8+, que permite
 * processamento paralelo sobre os elementos de colecoes.
 *
 * Seu uso eh indicado quando se tem um grande (mesmo) numero de elementos
 * e o processamento de um elemento nao gera interferencia no outro.
 *
 */
public class ParallelStreams {

    public static void main(String[] args) {
        serial();
        paralelo();
    }

    public static long fatorial (long numero) {
        long fat = 1;

        for (long i = numero; i > 1; i--) {
            fat*=i;
        }
        return fat;
    }

    /**
     *  Calcula o fatorial, de forma serial, para uma lista de
     *  numeros e imprime o tempo de execucao.
     */
    public static void serial()
    {
        long tempoInicio = System.currentTimeMillis();
        IntStream
                .range(0, 100000)
                .forEach(numero -> fatorial(numero));
        long tempoFinal = System.currentTimeMillis();

        System.out.println("Tempo de execucao serial: " + (tempoFinal-tempoInicio) + "ms");
    }

    /**
     *  Calcula o fatorial, de forma paralela, por meio do metodo parallel,
     *  para uma lista de numeros e imprime o tempo de execucao.
     */
    public static void paralelo()
    {
        long tempoInicio = System.currentTimeMillis();
        IntStream
                .range(0, 100000)
                .parallel()
                .forEach(numero -> fatorial(numero));
        long tempoFinal = System.currentTimeMillis();

        System.out.println("Tempo de execucao paralela: " + (tempoFinal-tempoInicio) + "ms");
    }
}
