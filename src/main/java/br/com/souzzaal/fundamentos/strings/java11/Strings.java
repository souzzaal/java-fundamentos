package br.com.souzzaal.fundamentos.strings.java11;

public class Strings {

    public static void main(String[] args) {

        // Retorna true a string tem tamanho zero ou contem apenas espacos
        System.out.println("isBlank? " + "                    ".isBlank());
        System.out.println("isBlank? " + "          A         ".isBlank());


        System.out.println("Andre".repeat(10)); // Concatena a String com ela mesma o numero de vezes passado

    }
}
