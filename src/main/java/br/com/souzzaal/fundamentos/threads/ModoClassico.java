package br.com.souzzaal.fundamentos.threads;

/**
 * Exemplo do uso classico de threads em Java.
 *
 * A Aplicacao simula a criacao de um PDF em uma thread, e
 * a exibicao de uma mensagem de aguarde em outra. A thread
 * que exibe a mensagem deve executar apenas durante a criacao
 * do PDF.
 *
 */
public class ModoClassico {
    public static void main(String[] args) {
        GeradorPDF geradorFake = new GeradorPDF();

        geradorFake.start(); // inicia a thread 'GeradorPDF'
    }
}

class GeradorPDF extends Thread {

    Aguarde aguarde;
    public GeradorPDF() {
        this.aguarde = new Aguarde(this);
    }

    public void run(){
        System.out.println("Gerando PDF");
        this.aguarde.start(); // inicia a thread 'Aguarde'

        try {
            Thread.sleep(10000); // simula o tempo de criacao do PDF
            System.out.println("PDF Gerado!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Aguarde extends Thread {

    Thread geradorPDF;
    private String mensagem = "Aguarde";

    public Aguarde(Thread gerador) {
        this.geradorPDF = gerador;
    }

    public void run () {
        while (true) {
            try {
                Thread.sleep(400);

                if(!this.geradorPDF.isAlive()) {
                    break;
                }

                System.out.println(this.getMensagemDinamica());

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Logica simplista para criar uma animacao na exibicao da mensagem "Aguarde"
     *
     * @return String mensagem
     */
    public String getMensagemDinamica() {
        if(this.mensagem.contains("...")) {
            this.mensagem = "Aguarde ";
        }
        this.mensagem = this.mensagem.concat(".");

        return this.mensagem;
    }
}