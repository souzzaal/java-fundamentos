package br.com.souzzaal.fundamentos.collections.set;


import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *  Quando utilizar:
 *      - Quando eh necessario manter a ordem de insercao dos elementos
 *
 *   Ordenacao:
 *      - Mantem a ordem de insercao
 *
 *   Performance:
 *      - Por manter ordenado eh a mais lenta
 */
public class LinkedHashSets {

    public static void main(String[] args) {
        Set<Integer> numeros = new LinkedHashSet<>();

        numeros.add(3);
        numeros.add(1);
        numeros.add(7);
        numeros.add(0);
        System.out.println("Mantida a ordem de insercao: " + numeros);

        numeros.remove(7);
        System.out.println("Apos remover o 7: " + numeros);

        // Iterando sobre a collection
        for (Integer item : numeros) {
            System.out.println("Iteracao com for: " + item);
        }

        Iterator<Integer> iterator = numeros.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iteracao com while/iterator: " + iterator.next());
        }
        System.out.println("Iteracao nao remove elementos: " + numeros);

        System.out.println("Quantidade de elementos: " + numeros.size());

        System.out.println("Conjunto vazio: " + numeros.isEmpty());

    }

}
