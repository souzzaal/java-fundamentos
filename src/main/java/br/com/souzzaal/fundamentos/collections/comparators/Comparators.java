package br.com.souzzaal.fundamentos.collections.comparators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Permite criar regras para ordenacao de objetos complexos
 */
public class Comparators {

    public static void main (String[] args) {

        List<Carro> carros = new ArrayList<>();

        carros.add(new Carro("Ferrari", "California", 2017));
        carros.add(new Carro("Porsche", "718 Boxster", 2017));
        carros.add(new Carro("Chevrolet", "Camaro SS", 2019));
        carros.add(new Carro("Audi", "R8-Spider", 2015));
        carros.add(new Carro("Nissan", "GT-R", 2020));
        carros.add(new Carro("Ford", "Mustang GT500", 2013));

        System.out.println("Carros pela ordem de insercao: "  + carros);

        Collections.sort(carros); //Collections.sort recebe uma lista que implementa comparable
        System.out.println("Ordenacao pela marca usando Comprable: " + carros);

        carros.sort(new CarroAnoComparator());
        System.out.println("Ordenacao por ano usando Comparator: " + carros);


        /* Abaixo novas formas de ordenacao partir do Java 8 */

        // Uso de expressoes lambda
        carros.sort((first, second) -> first.getModelo().compareTo(second.getModelo()));
        System.out.println("Ordenacao pelo modelo com Lambda: " + carros);

        carros.sort((first, second) -> second.getModelo().compareTo(first.getModelo()));
        System.out.println("Ordenacao inversa pelo modelo com Lambda: " + carros);

        // Usando Comparator
        carros.sort(Comparator.comparingInt(Carro::getAno));
        System.out.println("Ordenacao por ano usando comparingInt: " + carros);

        carros.sort(Comparator.comparingInt(Carro::getAno).reversed());
        System.out.println("Ordenacao inversa por ano usando comparingInt: " + carros);
    }

}
