# JPA

**Java Persistence API** é a especificação do Java para implementações ORM, ela fornece um comportamento comum para frameworks ORM Java. Para fazer a comunicação com o banco de dados é necessário utilizar uma das implementações do JPA, tais como HIbernate e EclipeLink.

## Configuração do projeto

### Adicionar as dependências do JPA e driver JDBC para o banco de dados a ser utilizado.

No caso deste projeto, utiliza-se gradle, assim na seção de dependências do arquivo `build.gradle` adicionar:

```
dependencies {

    // Drive JDBC que sera utilizado pelos frameworks que implementam o JPA
    // https://mvnrepository.com/artifact/mysql/mysql-connector-java
    compile group: 'mysql', name: 'mysql-connector-java', version: '8.0.17'

    //Essas sao as implementacoes do JPA (Hibernate e EclipseLink) e o automatizador de criacao de Metamodels ===

    // Implementacao Hibernate
    // https://mvnrepository.com/artifact/org.hibernate/hibernate-core
    compile group: 'org.hibernate', name: 'hibernate-core', version: '5.4.12.Final'

    // Implementacao EclipseLink
    // https://mvnrepository.com/artifact/org.eclipse.persistence/eclipselink
    //compile group: 'org.eclipse.persistence', name: 'eclipselink', version: '2.7.6'

    // Automatizador de criacao de Metamodels
    // https://mvnrepository.com/artifact/org.hibernate/hibernate-jpamodelgen
    // annotationProcessor('org.hibernate:hibernate-jpamodelgen:5.4.13.Final')
}
```

Pode-se utilizar tanto a implementação do Hibernate (a mais utilizada) quanto a do EclipseLink (a implementação oficial) para a especificação JPA.

 O Automatizador de criação de Metamodels é utilizado quando se utiliza o `JPA Criteria API` para consulta ao banco de dados. Não será utilizado neste projeto. 

### Criar arquivo persistence.xml

O arquivo `persistence.xml` fica em `/resources/META-INF`. Exemplo de possíveis configurações:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence http://xmlns.jcp.org/xml/ns/persistence/persistence_2_2.xsd"
             version="2.2">

    <!-- Unidade de persistencia -->
    <persistence-unit name="teste">

        <description> Unidade de persistencia de JPA (Hibernate ou EclipseLink) </description>

        <!-- PASSO 1 - Definir o provider de Implementacao do JPA -->
        <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
        <!--  <provider>org.eclipse.persistence.jpa.PersistenceProvider</provider> -->

        <!-- PASSO 2 - Definir as Classes (entidades) que serao mapeadas -->
        <class>path.to.class.ClassName1</class>
        <class>path.to.class.ClassName2</class>

        <!-- PASSO 3 - Configuracoes de conexao ao BD e do Hibernate/EclipseLink -->
        <properties>
            <!-- PASSO 3.1 - Configuracoes do banco de dados -->
            <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost/digital_innovation_one" />
            <property name="javax.persistence.jdbc.user" value="root" />
            <property name="javax.persistence.jdbc.password" value="password" />
            <property name="javax.persistence.jdbc.driver" value="com.mysql.cj.jdbc.Driver" />

            <!-- PASSO 3.2 - Configuracoes do Hibernate (os parametros so sao reconhecidos se estiver usando a implementacao do Hibernate)-->
            <property name="hibernate.dialect" value="org.hibernate.dialect.MySQL8Dialect" />
            <property name="hibernate.show_sql" value="true" />
            <property name="hibernate.format_sql" value="true" />
            <property name="hibernate.hbm2ddl.auto" value="create" />  <!-- Possible values for hibernate.hbm2ddl.auto are: validate, update, create, create-drop -->

            <!-- Configuracoes do EclipseLink (os parametros so sao reconhecidos se estiver usando a implementacao do EclipseLink) -->
<!--            <property name="eclipselink.target-database" value="MySQL"/>-->
<!--            <property name="eclipselink.logging.level.sql" value="FINE" />-->
<!--            <property name="eclipselink.logging.parameters" value="true" />-->
<!--            <property name="eclipselink.ddl-generation" value="drop-and-create-tables" />-->
        </properties>

    </persistence-unit>

</persistence>
```

Atenção: as configurações `<property name="hibernate.hbm2ddl.auto" value="create" />` e `<property name="eclipselink.ddl-generation" value="drop-and-create-tables" />` forçam a alteração do banco conforme a classe mapeada mude. Para os exemplos apresentados a base de dados será destruída e recriada toda vez que a aplicação for executada. 

### Anotar as classes mapeadas

Para que as classes mapeadas, indicadas com `<class>path.to.class.ClassName</class>` no `persistence.xml` possam de fato estar aptas para persistir dados é necessário que tenham anotações Java específicas, como por exemplo as apresentadas no exemplo a seguir.

```java
import javax.persistence.*;

@Entity    // Obrigatório. Indica que uma classe Java pode persistir no BD
@Table(name="pessoas") // informa o nome da tabela, por padrão JPA usa o nome da classe
public class Pessoa {

    @Id    // Obrigatório. JPA obriga a existência de uma chave primária
    @GeneratedValue(strategy= GenerationType.IDENTITY) // Transfere para o BD a resposabilidade de criar a PK
    private int id;

    @Column(nullable = false) // @column é opcional, mas pode ser usada para definir detalhes dos campos
    private String nome;

    @Column(name = "data_nascimento") // informa o nome do campo na tabela
    private String dataNascimento;

    @ManyToOne(fetch = FetchType.LAZY) // Anotação de relacionamento, indicando forma de carregamento do campo/tabela associada
    private Estado estado;

    // ... construtores e métodos da classe
}
```

## Usando JPA

Para usar o JPA é preciso carregar a configuração do arquivo `persistence.xml`, sendo que o argumento passado para `createEntityManagerFactory()` deve ser o nome unidade de persistência definida na *tag* `<persistence-unit name="name">` deste arquivo. 

```java
  EntityManagerFactory factory = Persistence.createEntityManagerFactory("teste");
  EntityManager manager = factory.createEntityManager();

  Pessoa pessoa = new Pessoa("André");
  manager.getTransaction().begin();  // Abre a transação
  manager.persist(pessoa);
  manager.getTransaction().commit(); // Confirma a persistencia dos dados
  System.out.println("ID da pessoa: " + pessoa.getId());
  manager.close()
```

JPA só permite persistir dados se a manipulação estiver dentro de uma transação.

O `EntityManager` apresentado acima tem a limitação de poder operar apenas sobre um objeto, para podermos manipular uma coleção de objetos podemos utilizar o `JPQL`:

```java
// Trazendo somente 1 resultado
String jpql = "SELECT p FROM pessoas p WHERE p.nome = :nome";
Aluno pessoaJPQL = entityManager
        .createQuery(jpql, Aluno.class)
        .setParameter("nome", nome)
        .getSingleResult();

// Trazendo uma lista como resultado
String jpqlList = "SELECT p FROM pessoas p JOIN p.estado e WHERE e.nome = :estadoNome";
List<Aluno> pessoaJPQLList = entityManager
        .createQuery(jpqlList, Aluno.class)
        .setParameter("estadoNome", pessoaJPQL.estado.getNome())
        .getResultList();

// Resultados das consultas acima
System.out.println("Consulta pessoaJPQL: " + alunoJPQL);
pessoaJPQLList.forEach(Pessoa -> System.out.println("Consulta pessoaJPQLList: " + Pessoas));
```

JPQL trás muitas facilidades em relação ao EntityManager, como por exemplo, a capacidade de utilizar notação de orientação a objetos na declaração da instrução de consulta ao  banco, que será transformada para SQL em tempo de execução. 

## Referências

- [GitHub - danielkv7/jpa-basico](https://github.com/danielkv7/jpa-basico) 

- [Uma introdução prática ao JPA com Hibernate](https://www.caelum.com.br/apostila-java-web/uma-introducao-pratica-ao-jpa-com-hibernate)

- [Como fazer um Persistence.xml modelo ](https://javasemmisterios.com.br/como-fazer-um-persistence-xml-modelo/)

- [Java Persistence/JPQL](https://en.wikibooks.org/wiki/Java_Persistence/JPQL)