package br.com.souzzaal.fundamentos.http.java11;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 *
 *  O Java 11 simplifica a criacao de requisicoes HTTP com o recurso nativo HttpClient;
 *
 */
public class HttpClientApi {
    public static void main(String[] args) {
        fazRequisicaoUsandoHttpClient();
    }

    public static void fazRequisicaoUsandoHttpClient()
    {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://my-json-server.typicode.com/typicode/demo/posts"))
                .build();

        var httpClient = HttpClient.newHttpClient();

        HttpResponse<String> response = null;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Status code: " + response.statusCode());
        System.out.println("Headers: " + response.headers());
        System.out.println("Conteudo: " + response.body());
    }

}
