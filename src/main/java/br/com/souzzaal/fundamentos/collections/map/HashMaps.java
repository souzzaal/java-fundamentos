package br.com.souzzaal.fundamentos.collections.map;

import java.util.HashMap;
import java.util.Map;

/**
 * As colecoes do tipo Map no Java implementam as funcionalidades de Tabela Hash, que recebem
 * dois valores de entrada: a chave (de indexacao) e o objeto.
 *
 * Permite valores repetidos, mas nao chaves
 *
 * Permite adicao, busca por chave e valor, atualizacao, remocao, navegacao e ordenacao.
 *
 * */

public class HashMaps {

    public static void main(String[] args) {
        Map<String, Integer> campeoesMundialFifa = new HashMap<>();

        // Adiciona itens ao mapa
        campeoesMundialFifa.put("Brasil", 4);
        campeoesMundialFifa.put("Alemanha", 4);
        campeoesMundialFifa.put("Italia", 4);
        campeoesMundialFifa.put("Uruguai", 2);
        campeoesMundialFifa.put("Argentina", 2);
        campeoesMundialFifa.put("Franca", 2);
        campeoesMundialFifa.put("Inglaterra", 1);
        campeoesMundialFifa.put("Espanha", 1);
        System.out.println("Adicionaodos itens ao mapa: " + campeoesMundialFifa);

        // Atualiza valor no mapa
        campeoesMundialFifa.put("Brasil", 5);
        System.out.println("Atualizado valor Brasil: " + campeoesMundialFifa);

        // Retorna o valor para uma chave
        System.out.println("Quantidade de titulos da Argentina: " + campeoesMundialFifa.get("Argentina"));

        // Verfica existencia de chave
        System.out.println("Existe Portugal: " + campeoesMundialFifa.containsKey("Portugal"));

        // Verifica existencia de valor (nao performatico)
        System.out.println("Existe pentacampeao: " + campeoesMundialFifa.containsValue(5));

        // Remove um item do mapa
        campeoesMundialFifa.remove("Franca");
        System.out.println("Removendo a Franca: " + campeoesMundialFifa);

        // Tamanho do mapa
        System.out.println("Numero de selecoes com mundial: " + campeoesMundialFifa.size());

        for(Map.Entry<String, Integer> entry : campeoesMundialFifa.entrySet()) {
            System.out.println("Iterando com entreSet: " + entry.getKey() + " => " + entry.getValue());
        }

        for (String key : campeoesMundialFifa.keySet()) {
            System.out.println("Iterando com keySet: " + key + " => " + campeoesMundialFifa.get(key));
        }

        // Limpa o mapa
        campeoesMundialFifa.clear();
        System.out.println("Mapa limpo: " + campeoesMundialFifa);

    }
}
