package br.com.souzzaal.fundamentos.jdbc;
import jdk.swing.interop.SwingInterOpUtils;

import javax.servlet.ServletOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class App {

    public static void main(String[] args) {
        var cursoDao = new CursoDAO();

        System.out.println("\nCadastrando curso: Java - 120hs");
        cursoDao.create(new Curso(0, "Java", (float) 120.0));

        System.out.println("Cadastrando curso: JavaScript - 120hs");
        cursoDao.create(new Curso(0, "JavaScript", (float) 100.0));

        System.out.println("Cadastrando curso: HTML - 65.5hs");
        cursoDao.create(new Curso(0, "HTML", (float) 65.5));


        var cursos = cursoDao.list();
        System.out.println("\nListando todos os cursos");
        cursos.stream().forEach(curso -> System.out.println(curso));

        System.out.println("\nPesquisando pelo id (" + cursos.get(0).getId() + ")");
        var primeiroCurso = cursoDao.getById(cursos.get(0).getId());
        System.out.println(primeiroCurso);

        System.out.println("\nAtualizando curso (" + cursos.get(0).getId() + ")");
        primeiroCurso.setNome("Java 10");
        primeiroCurso.setDuracaoHoras((float) 150);
        cursoDao.update(primeiroCurso);

        cursos = cursoDao.list();
        System.out.println("\nListando todos os cursos");
        cursos.stream().forEach(curso -> System.out.println(curso));

        System.out.println("\nDeletando o ultimo curso ("+cursos.get(cursos.size()-1).getId()+ ")");
        cursoDao.delete(cursos.get(cursos.size()-1).getId());

        cursos = cursoDao.list();
        System.out.println("\nListando todos os cursos");
        cursos.stream().forEach(curso -> System.out.println(curso));

        System.out.println("\nTruncando a tabela cursos");
        cursoDao.truncate();
    }

}
