package br.com.souzzaal.fundamentos.collections.comparators;

/**
 * A interface comparable permite criar na classe de negocio a regra
 * para ordenacao de uma lista do objeto.
 *
 * Para esta classe, pelo metodo compareTo, ordena-se uma lista de Carros
 * de forma ascendete pela sua marca.
 *
 */
public class Carro implements Comparable<Carro> {

    private final String marca;
    private final String modelo;
    private final Integer ano;

    public Carro(String marca, String modelo, Integer ano) {
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
         }

    @Override
    public int compareTo(Carro o) {
        return this.marca.compareTo(o.marca);
    }

    public String toString() {
        return this.marca + " " + this.modelo + " " + this.ano;
    }

    public String getMarca() {
        return this.marca;
    }

    public String getModelo() {
        return this.modelo;
    }

    public Integer getAno() {
        return this.ano;
    }
}
