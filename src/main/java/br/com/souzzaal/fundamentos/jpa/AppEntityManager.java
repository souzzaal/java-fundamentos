package br.com.souzzaal.fundamentos.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Estudando interação com o banco de dados usando EntityManager
 */
public class AppEntityManager {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-teste");
        EntityManager manager = factory.createEntityManager();

        Curso java = new Curso("Java", (float) 150.0);
        Curso javaScript = new Curso("JavaScript", (float) 100.0);
        Curso go = new Curso("Go", (float) 50.0);

        Aluno andre = new Aluno("Andre");
        andre.adicionaCurso(java);
        andre.adicionaCurso(javaScript);
        andre.adicionaCurso(go);
        Aluno luiz = new Aluno("Luiz");
        luiz.adicionaCurso(javaScript);

        manager.getTransaction().begin();

        manager.persist(java);
        manager.persist(javaScript);
        manager.persist(go);

        manager.persist(andre);
        manager.persist(luiz);

        manager.getTransaction().commit();

        System.out.println("\n\n============= Adicionando cursos ==============");

        System.out.println("\n\n Cursos:");
        System.out.println("\t >> Java: " + java);
        System.out.println("\t >> JavaScript: " + javaScript);

        System.out.println("\n\n============= Adicionando cursos ==============");
        System.out.println("\nAlunos:");
        System.out.println("\t >> Andre: " + andre);
        System.out.println("\t >> Luiz: " + luiz);

        System.out.println("\n\n============= Buscando aluno ==============");
        luiz = null;
        luiz = manager.find(Aluno.class, 2);
        System.out.println("\t >> Luiz (recuperado do banco): " + luiz);

        System.out.println("\n\n============= Editando e removendo ==============");
        manager.getTransaction().begin();
        // Objetos alterados dentro de uma transação terão seu novo estado persistido no banco.
        go.setNome("Introducao ao Go");         // edita o nome do curso curso
        andre.removeCurso(go);                  // edita remove o curso 'go' do aluno 'andre'
        manager.remove(luiz);                   // excluindo aluno 'luiz'
        manager.getTransaction().commit();
        System.out.println("\t >> Andre: " + andre);
        System.out.println("\t >> Go: " + go);

        manager.close();
    }

}
