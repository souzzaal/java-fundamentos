package br.com.souzzaal.fundamentos.java10;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Recurso disponibilizado no Java 10 que torma menos verbosa a escrita de codigo
 * quando permite o uso da palavra reservada 'var' para subustituir o tipo da variavel.
 * <p>
 * - var nao pode ser utilizado em contexto de classe, pode ser usado apenas em escopo local (dentro de metodo).
 * - var nao pode ser utilizada em assinatura de metodos
 * - var nao pode ser utilizada em variaveis nao inicializadas
 * - var pode ser utilizada em laços for
 * - var pode ser utilizada em try resources
 */
public class Inferencia {


    public static void main(String[] args) {
        basico();
        lacos();
        tryResource();
    }

    public static void basico() {
        var texto = "Andre";
        var numero = new Integer(10);
        var classe = new Pessoa();

        System.out.println("Inferencia String: " + texto.getClass());
        System.out.println("Inferencia Integer: " + numero.getClass());
        System.out.println("Inferencia Classe: " + classe.getClass());
    }

    public static void lacos() {
        int[] numeros = {1, 2, 3, 4, 5};

        for (var numero : numeros) {
            System.out.println("For:  " + numero);

            for (var i = 0; i < numero; i++) { // funciona por causa do escopo
                System.out.println('.');
            }
        }
    }

    /**
     * O que é: um recurso sintático do Java para uso seguro de recursos de forma segura.
     * <p>
     * Objetivo: garantir que recursos escassos - como conexões com o banco de dados,
     * referências a arquivos, conexões de rede - sejam devidamente fechadas após o uso,
     * mesmo num cenário excepcional.
     * <p>
     * Funcionamento: os recursos declarados no try (entre os parêntesis) devem implementar a
     * interface AutoCloseable e terão seu método close() automaticamente chamado ao final do bloco try.
     * <p>
     * Benefícios:
     * <p>
     * - Substitui o tratamento manual de exceções:
     * - Menos propenso a erros de codificação, quando o programador não sabe ou esquece de executar todo o tratamento necessário.
     * - Evita vazamento de recursos, quando o programador esquece de fechá-lo ou não trata corretamente uma situação excepcional.
     * - Menos código boilerplate:
     * - Facilita e agiliza a codificação.
     * - Menos chance de esquecer algo.
     * - Menos código significa menos bugs e menos coisas para dar manutenção.
     * <p>
     * Fonte: https://pt.stackoverflow.com/questions/172909/como-funciona-o-try-with-resources
     */
    public static void tryResource() {

        // o var na linha abaixo substitui o tipo BufferedReader
        try (var br = new BufferedReader(new FileReader("arquivoInexistente.txt"))) {
            System.out.println(br.readLine());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

class Pessoa {

}