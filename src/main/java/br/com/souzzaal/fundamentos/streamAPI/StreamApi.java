package br.com.souzzaal.fundamentos.streamAPI;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * StreamAPI permite manipular uma colecao de dados (executando um algoritmo sobre os cada elemento),
 * com paradigma funcional de forma paraelela.
 *
 * Imutavel: nao altera a colecao de origem, sempre cria uma nova colecao.
 *
 * Deve ser usada para tratar grandes colecoes de dados, principalmente quando se deseja
 * performance.
 *
 */
public class StreamApi {

    public static void main(String[] args) {
        List<String> pessoas = new ArrayList<>();

        pessoas.add("Pedro");
        pessoas.add("Joao");
        pessoas.add("Tiago");
        pessoas.add("Filipe");
        pessoas.add("Mateus");
        pessoas.add("Andre");
        pessoas.add("Simao");

        System.out.println("Lista de pessoas: " + pessoas);

        System.out.println("Contagem de elementos: " + pessoas.stream().count());

        System.out.println("Maior nome: " + pessoas.stream().max(Comparator.comparingInt(String::length)));

        System.out.println("Menor nome: " + pessoas.stream().min(Comparator.comparingInt(String::length)));

        /*
            Filter retorna um stream, usamos um collector para retornar uma lista.
            Existem diversos collectors para Lista, Set, String, dentre outros.
        */
        System.out.println("Filtrando por nomes que contem 'a': " +
                pessoas.stream()
                        .filter((pessoa) -> pessoa.toLowerCase().contains("a"))
                        .collect(Collectors.toList())
        );

        /*
            Para fins didaticos: Map executa um processamento sobre cada item da colecao,
            retornando um novo stream de igual tamanho com o resultados da operacao
        */
        System.out.println("Exibindo o total de letras do nome: " +
            pessoas.stream()
                    .map(pessoa -> pessoa.concat(": ").concat(String.valueOf(pessoa.length())))
                    .collect(Collectors.toList())
        );

        System.out.println("Top 3: "  +
            pessoas.stream()
            .limit(3)
            .collect(Collectors.toList())
        );

        /*
            Peek executa algum processamento (nesse caso exibir cada item)
            e retorna a mesma stream
        */
        System.out.println("Collection 'retornada' apos peek: " +
            pessoas.stream()
            .peek(System.out::println)
            .collect(Collectors.toList())
        );

        /*
            forEach apenas executa um processamento e nao retorna nada.
            Como nao permite encadeamento a acao acaba, neste caso,
            na exibicao dos nomes
        */
        pessoas.stream().forEach((nome) -> System.out.println("forEach: " + nome));

        System.out.println("Todas as pessoas tem 'o' no nome? " +
            pessoas.stream()
            .allMatch((nome) -> nome.contains("o"))
        );

        System.out.println("Pelo menos uma pessoa tem 'o' no nome? " +
            pessoas.stream()
            .anyMatch((nome) -> nome.contains("o"))
        );

        System.out.println("Nenhuma pessoa possui 'z' no nome? " +
            pessoas.stream()
            .noneMatch((nome) -> nome.contains("z"))
        );

        System.out.println("Alguma pessoa tem 'n' no nome? " +
            pessoas.stream()
            .anyMatch((nome) -> nome.contains("n"))
        );

        /*
            findFirst retorna um Optional (objeto Java 8+, que visa melhorar a manipulacao de
            de objetos que podem nulos), por isso usa-se o ifPresent, que verifica se existe conteudo
            valido, e neste caso, exibe o nome

        */
        pessoas.stream()
            .findFirst()
            .ifPresent((nome) -> System.out.println("Primeiro elemento: " + nome));

        // Encadeamento de operacoes
        System.out.println("Encadeia, filtra e retorna uma lista: " +
                pessoas.stream()
                .peek(System.out::println) // apenas exibe os nomes
                .map(pessoa -> pessoa.concat(": ").concat(String.valueOf(pessoa.length())))
                .peek(System.out::println) // apenas exibe os nomes
                .filter(pessoa -> pessoa.contains("5")) // filtra todos os nomes com '5'
                .collect(Collectors.toList()) // retorna uma lista
        );

        System.out.println("Encadeia e retorna um Set: " +
                pessoas.stream()
                .map(pessoa -> pessoa.concat(": ").concat(String.valueOf(pessoa.length())))
                .collect(Collectors.toSet())
        );

        System.out.println("Encadeia e retorna uma string: " +
                pessoas.stream()
                .map(pessoa -> pessoa.concat(": ").concat(String.valueOf(pessoa.length())))
                .collect(Collectors.joining(", ")) // concatena a string com ", "
        );

        System.out.println("Encadeia e retorna um agrupamento pela quantidade de caracteres: " +
                pessoas.stream()
                .collect(Collectors.groupingBy(pessoa -> pessoa.length()))
        );
    }
}
