package br.com.souzzaal.fundamentos.collections.queue;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 *  LinkedList implementa a funcionalidade de fila
 *
 *  - Garante ordem de insercao
 *  - Permite adicao, leitura, e remocao (considerando FIFO)
 *  - Nao permite ordenacao
 *
 */
public class LinkedLists {

    public static void main (String[] args) {
        Queue<String> fila = new LinkedList<>();

        // Adicionando elementos
        fila.add("Joao");
        fila.add("Maria");
        fila.add("Pedro");
        fila.add("Antonio");
        fila.add("Jose");
        System.out.println("Adicinando elementos a fila: " + fila);

        // Consultado o proximo elemento a sair da fila
        var primeiro = fila.peek();
        System.out.println("Consulta primeiro da fila: " + primeiro);

        // Consultado e removendo o primero da fila
        primeiro = fila.poll();
        System.out.println("Remove primeiro da fila: " + primeiro);
        System.out.println("Fila apos remocao: " + fila);


        // Iterando sobre a collection
        for (String item : fila) {
            System.out.println("Iteracao com for: " + item);
        }

        Iterator<String> iterator = fila.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iteracao com while/iterator: " + iterator.next());
        }
        System.out.println("Iteracao nao remove elementos: " + fila);

        System.out.println("Quantidade de elementos: " + fila.size());

        fila.clear();
        System.out.println("Fila vazia: " + fila.isEmpty());

        try {
            System.out.println("Obtendo o primeiro ou erro: " + fila.element());
        } catch (Exception e) {
            System.out.println("Obtendo o primeiro ou erro: " + e);
        }

    }

}
