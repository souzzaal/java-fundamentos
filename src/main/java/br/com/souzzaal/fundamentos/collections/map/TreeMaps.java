package br.com.souzzaal.fundamentos.collections.map;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Parecido com o TreeSet, no sentido de que usa uma arvore binaria para manter a ordenacao
 * dos elementos, mas tomando como base a chave de indexacao.
 *
 * Util em cenarios em que nao se tem preocupacao com performance, mas com ordenacao
 *
 */
public class TreeMaps {

    public static void main(String[] args) {

        TreeMap<String, String> treeCapitais = new TreeMap<>();

        // Adiciona itens a arvoce
        treeCapitais.put("RS", "Porto Alegre");
        treeCapitais.put("BA", "Salvador");
        treeCapitais.put("SC", "Florianopolis");
        treeCapitais.put("PR", "Curitiba");
        treeCapitais.put("SP", "Sao Paulo");
        treeCapitais.put("RJ", "Rio de Janeiro");
        treeCapitais.put("MG", "Belo Horizonte");

        System.out.println("Adiciona itens a arvore: " + treeCapitais);

        // Retorna o primeira da arvore
        System.out.println("Primeira chave da arvore: " + treeCapitais.firstKey());

        // Retorna o ultmo  item da arvore
        System.out.println("Ultima chave da arvore: " + treeCapitais.lastKey());

        // Retorna a primeira chave acima da chave passada
        System.out.println("Chave acima de SC: " + treeCapitais.higherKey("SC"));

        // Retorna a primeira chave abaixo da chave passada
        System.out.println("Chave abaixo de SC: " + treeCapitais.lowerKey("SC"));

        // Retorna primeiro objeto da arvore
        System.out.println("Acesando primeiro objeto da arvore com Entry: " + treeCapitais.firstEntry().getKey() + " => " + treeCapitais.firstEntry().getValue());

        // Retorna a ultimo objeto da arvore
        System.out.println("Acesando ultimo objeto da arvore com Entry: "  + treeCapitais.lastEntry().getKey() + " => " + treeCapitais.lastEntry().getValue());

        // Retorna o primeiro objeto acima, com base na chave passada
        System.out.println("Primeiro objeto acima de SC: "  +treeCapitais.higherEntry("SC").getKey() + " => " + treeCapitais.higherEntry("SC").getValue());

        // Retorna o primeiro objeto abaixo, com base na chave passada
        System.out.println("Primeiro objeto abaixo de SC: " + treeCapitais.lowerEntry("SC").getKey() + " => " + treeCapitais.lowerEntry("SC").getValue());

        // Retorna primeiro intem da arvore, removendo do map
        Map.Entry<String, String> firstEntry = treeCapitais.pollFirstEntry();
        System.out.println("Retornando e removendo primerio no: " + firstEntry.getKey() + " => " + firstEntry.getValue());

        // Retorna ultimo item da arvore, removendo do map
        Map.Entry<String, String> lastEntry = treeCapitais.pollLastEntry();
        System.out.println("Retornando e removando ultimo no: " + lastEntry.getKey() + " => " + lastEntry.getValue());

        System.out.println("Apos remocao: " +  treeCapitais);

        // Iteracoes
        Iterator<String> iterator = treeCapitais.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println("Itenrando com while: " +  key + " => " + treeCapitais.get(key));
        }

        for (String capital: treeCapitais.keySet()) {
            System.out.println("Iterando com for/keySet: " + capital + " => " + treeCapitais.get(capital));
        }

        for ( Map.Entry<String, String> capital: treeCapitais.entrySet()) {
            System.out.println("Iterando com for/entry" + capital.getKey() + " => " + capital.getValue());
        }

        System.out.println("Tamanho da arvore: " + treeCapitais.size());

        System.out.println("Arvore vazia: " + treeCapitais.isEmpty());

        System.out.println("Checa existencia de chave 'MG': " + treeCapitais.containsKey("MG"));

        System.out.println("Checa existencia de conteudo 'Bahia': " + treeCapitais.containsKey("Bahia"));

        treeCapitais.clear();

        System.out.println("Arvore limpa: " + treeCapitais);
    }

}
