
package br.com.souzzaal.fundamentos.jpa;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "alunos")
public class Aluno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String nome;

	@ManyToMany
	@JoinTable(
			name = "alunos_cursos",
			joinColumns = @JoinColumn(name = "alunos_id"),
			inverseJoinColumns = @JoinColumn(name = "cursos_id")
	)
	List<Curso> cursos = new ArrayList<>();

	public Aluno() {
	}

	public Aluno(String nome) {
		this.nome = nome;
	}

	public void adicionaCurso(Curso curso) {
		this.cursos.add(curso);
	}

	public void removeCurso(Curso curso) {
		this.cursos = this.cursos
				.stream()
				.filter(c -> c.getId() != curso.getId())
				.collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return "Aluno{" +
				"id=" + id +
				", nome='" + nome + '\'' +
				", cursos=" + cursos +
				'}';
	}
}
