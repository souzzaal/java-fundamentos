package br.com.souzzaal.fundamentos.collections.comparators;

import java.util.Comparator;

/**
 * A inteface comparator permite criar, fora da classe de negocio,
 * a regra para ordenacao de uma lista do objeto. Desta forma, torna-se
 * possivel criar varias estrategias de ordenacao.
 *
 */
public class CarroAnoComparator implements Comparator<Carro> {

    @Override
    public int compare(Carro o1, Carro o2) {
        return o1.getAno() - o2.getAno();
    }
}
