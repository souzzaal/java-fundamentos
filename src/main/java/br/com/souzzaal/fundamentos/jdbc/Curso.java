package br.com.souzzaal.fundamentos.jdbc;

public class Curso {

    private int id;
    private String nome;
    private Float duracaoHoras;

    public Curso(int id, String nome, Float duracao) {
        this.id = id;
        this.nome = nome;
        this.duracaoHoras = duracao;
    }

    public Curso() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Float getDuracaoHoras() {
        return duracaoHoras;
    }

    public void setDuracaoHoras(Float duracao_horas) {
        this.duracaoHoras = duracao_horas;
    }

    @Override
    public String toString() {
        return "Curso{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", duracao_horas=" + duracaoHoras +
                '}';
    }
}
