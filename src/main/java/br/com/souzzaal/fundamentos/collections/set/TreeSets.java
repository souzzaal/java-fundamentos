package br.com.souzzaal.fundamentos.collections.set;

import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 *  Quando utilizar:
 *      - Quando eh necessario alterar a ordem atraves de Compsrators
 *
 *   Ordenacao:
 *      - Mantem a ordem e pode ser reordenado
 *      - Mantem a ordem atraves do balanceamento de uma arvore binaria, realizado a cada modificacao no Set
 *
 *   Performance:
 *      - Eh performatico para a leitura.
 *      - Para modificacao tem a necessidade de reordenar, sendo mais lenta que o LinkedHashSet
 */
public class TreeSets {

    public static void main(String[] args) {

        TreeSet<String> capitais = new TreeSet<>();
        capitais.add("Salvador");
        capitais.add("Sao Paulo");
        capitais.add("Belo Horizonte");
        capitais.add("Rio de Janeiro");
        capitais.add("Recife");
        capitais.add("Fortaleza");

        System.out.println("Elementos ordenados na insercao: " + capitais);

        System.out.println("Primeiro elemento da arvore: " + capitais.first());

        System.out.println("Ultimo elemento da arvore: " + capitais.last());

        System.out.println("Elemento acima de Salvador: " + capitais.higher("Salvador"));

        System.out.println("Elemento abaixo de Salvador: " + capitais.lower("Salvador"));

        System.out.println("Retorna e remove primeiro elemento: " + capitais.pollFirst() + " -> " + capitais);

        System.out.println("Retorna e remove ultimo elemento: " + capitais.pollLast() + " -> " + capitais);


        // Iterando sobre a collection
        for (String item : capitais) {
            System.out.println("Iteracao com for: " + item);
        }

        Iterator<String> iterator = capitais.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iteracao com while/iterator: " + iterator.next());
        }
        System.out.println("Iteracao nao remove elementos: " + capitais);

        System.out.println("Quantidade de elementos: " + capitais.size());

        System.out.println("Conjunto vazio: " + capitais.isEmpty());

    }

}
