package br.com.souzzaal.fundamentos.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Estudando interação com o banco de dados usando JPQL
 */
public class AppJPQL {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-teste");
        EntityManager manager = factory.createEntityManager();

        Curso java = new Curso("Java", (float) 150.0);
        Curso javaScript = new Curso("JavaScript", (float) 100.0);
        Curso go = new Curso("Go", (float) 50.0);

        Aluno andre = new Aluno("Andre");
        andre.adicionaCurso(java);
        andre.adicionaCurso(javaScript);

        Aluno luiz = new Aluno("Luiz");
        luiz.adicionaCurso(java);
        luiz.adicionaCurso(javaScript);

        manager.getTransaction().begin();
        manager.persist(java);
        manager.persist(javaScript);
        manager.persist(go);

        manager.persist(andre);
        manager.persist(luiz);
        manager.getTransaction().commit();

        /*
            Em JPQL nao usamos "select *" usamos "select 'X'", onde 'X' eh o apelido dado a entidade.

            JPQL permite usar o nome da entidade mapeada ao inves do nome da tabela no banco. No exemplo
            abaixo "Aluno" eh o nome da classe Java.
        */
        String jpql = "SELECT a FROM Aluno a WHERE a.nome = :nome";
        Aluno alunoJPQL = manager
                .createQuery(jpql, Aluno.class)
                .setParameter("nome", "Andre")
                .getSingleResult(); // retorna apenas um resultado
        System.out.println("\n\nAlunoJPQL: " + alunoJPQL + "\n\n");

        /*
            Neste exemplo destaca-se a forma de fazer o JOIN: bastando referenciar a propriedade 'cursos'
            da classe Aluno. A sintaxe explora o uso de OO ao invés de SQL.
        */
        String jpqlList = "SELECT a FROM Aluno a JOIN a.cursos c WHERE c.nome = :nomeCurso";
        List<Aluno> alunosJPQLList = manager
                .createQuery(jpqlList, Aluno.class)
                .setParameter("nomeCurso", java.getNome())
                .getResultList();   // retorna uma lista de resultados que tem o curso 'Java'

        alunosJPQLList.forEach(Aluno -> System.out.println("Consulta alunoJPQLList: " + Aluno));

        manager.getTransaction().begin();
        String jpqlUpdate = "UPDATE Curso c SET c.nome = :nome WHERE c.id = :id";
        int linhaAfetada = manager
                .createQuery(jpqlUpdate)
                .setParameter("nome", "Introducao ao Go")
                .setParameter("id", go.getId())
                .executeUpdate();

       jpqlUpdate = "UPDATE Curso c SET c.duracaoHoras = c.duracaoHoras*2 WHERE c.id > 1";
       linhaAfetada = manager
                .createQuery(jpqlUpdate)
                .executeUpdate();
        manager.getTransaction().commit();

        manager.clear(); // Sem limpar o contexto a consulta abaixo nao faz os valores atualizados.
        List<Curso> cursosJPQLList = manager
                .createQuery("SELECT c FROM Curso c", Curso.class)
                .getResultList();
        cursosJPQLList.forEach(Curso -> System.out.println(">>>> Consulta cursosJPQLList: " + Curso));

        manager.getTransaction().begin();
        String jpqlDelete = "DELETE FROM Curso c WHERE c.id = :id";
        linhaAfetada = manager
                .createQuery(jpqlDelete)
                .setParameter("id", go.getId())
                .executeUpdate();
        manager.getTransaction().commit();

        cursosJPQLList = manager
                .createQuery("SELECT c FROM Curso c", Curso.class)
                .getResultList();
        cursosJPQLList.forEach(Curso -> System.out.println(">>>> Consulta cursosJPQLList: " + Curso));

        manager.close();
    }
}