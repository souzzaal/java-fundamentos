package br.com.souzzaal.fundamentos.collections.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 *  Quando utilizar:
 *      - Quando nao eh necessario manter a ordem de insercao
 *
 *   Ordenacao:
 *      - Nao permite ordenacao nem valores repetidos
 *
 *   Performance:
 *      - Por nao ter repeticao e nao permitir ordenacao eh a mais performatica
 */
public class HashSets {

    public static void main(String[] args) {
        Set<Double> notas =  new HashSet<>();

        notas.add(5.0);
        notas.add(8.2);
        notas.add(9.9);
        notas.add(7.6);
        notas.add(0.1);

        System.out.println("Adicionando elementos: " + notas);

        notas.remove(0.1);
        System.out.println("Removido 0.1: " + notas);

        // Iterando sobre a collection
        for (Double item : notas) {
            System.out.println("Iteracao com for: " + item);
        }

        Iterator<Double> iterator = notas.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iteracao com while/iterator: " + iterator.next());
        }
        System.out.println("Iteracao nao remove elementos: " + notas);

        System.out.println("Quantidade de elementos: " + notas.size());


    }

}
