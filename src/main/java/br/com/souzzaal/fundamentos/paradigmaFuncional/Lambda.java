package br.com.souzzaal.fundamentos.paradigmaFuncional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Funcoes lambda no java sao definidas a partir de interfaces funcionais e devem ter a seguinte
 * estrutura: InterfaceFuncional nomeVariavel = parametro -> logica
 *
 */
public class Lambda {

    public static void main(String[] args) {

        lambdaUmaLinha();
        lambdaCorpoLongo();
        lambdaMultiplosParametros();

        Calculo soma = (a, b) -> a + b;
        System.out.println("Funcao de alta ordem: " + funcaoDeAltaOrdem(soma, 5,5));

        funcoes();
        predicados();
        consumidores();
        supridores();

        iteracoes("Andre", "Luiz");
    }
    private static void lambdaUmaLinha()
    {
        // interface  nomeVariavel        parametro    logica
        RetornaString minhaFuncaoLambda = parametro -> parametro;
        System.out.println("Lambda uma linha: " + minhaFuncaoLambda.gerar("Andre"));

        RetornaString colocarPrefixoSenhorNaString = valor -> "Sr. " + valor;
        System.out.println("Lambda uma linha: " +  colocarPrefixoSenhorNaString.gerar("Andre"));
    }

    private static void lambdaCorpoLongo() {
        RetornaString minhaFuncaoLambdaComCorpoLongo = valor -> {
            String comPrefixo = "Sr. " + valor;
            String comPrefixoMaisPontoFinal = comPrefixo + ".";
            return comPrefixoMaisPontoFinal;
        }; // ; obrigatorio para funcoes lambda
        System.out.println("Lambda corpo longo: " + minhaFuncaoLambdaComCorpoLongo.gerar("Andre"));
    }

    private static void lambdaMultiplosParametros() {
        ConcatenaString minhaFuncaoLambdaComMaisDeUmParametro = (param1, param2) -> param1.concat(param2);
        System.out.println("Lambda multiplos parametros: " + minhaFuncaoLambdaComMaisDeUmParametro.concatena("Paradigma", "Funcional"));
    }

    /**
     * Funcao de alta ordem eh uma funcao que retorna ou recebe uma funcao como parametro.
     */
    private static int funcaoDeAltaOrdem(Calculo calculo, int a, int b)
    {
        return calculo.calcular(a, b);
    }

    /**
     * Recurso que permite criar funcoes lambda forçando os tipos dos argumentos e do retorno
     */
    private static void funcoes()
    {
        Function<Double, Double> fahrenheitParaCelsuis =  grau -> (grau - 32d) * (5d/9d);
        System.out.println("Functions - 77º Fahrenheit para Celsuis: " + fahrenheitParaCelsuis.apply(77.0));

        Function<Integer, String> number2String = integer -> integer.toString();
        System.out.println("Functions - converte inteiro (2) para string: " + number2String.apply(2));
    }

    /**
     * Recurso que permite criar funcoes lambda que recebe um argumento qualquer e retorna um
     * boleano.
     */
    private static void predicados()
    {

        Predicate<String> emailValido = string -> string.matches("^[a-zA-Z0-9+_.]+@[a-zA-Z0-9]+(\\.[a-z]+)+$");

        System.out.println("Predicados - 'user@mail.com' valido? " + emailValido.test("user@mail.com"));
        System.out.println("Predicados - 'user@mail' invalido? " + emailValido.test("user@mail"));
    }

    /**
     * Recurso que permite criar funcoes lambda que recebem argumentos e nao retorna nada
     */
    private static void consumidores()
    {
        Consumer<Object> printLog = object -> System.out.println("Consumer Log: " + object); // forma basica da lambda
        Consumer<Object> println = System.out::println; // metodo de referencia: forma curta de escrever, o a Java aplica o parametro na funcao do lado direito.

        printLog.accept(new ArrayList<>());
        println.accept("Consumer: Andre");
    }

    /**
     * Recurso que permite criar funcoes lambda que nao recebem parametros e retornam algo
     */
    private static void supridores()
    {
        Supplier<List> nomesComCincoLetras = () -> {
            List<String> pessoas = new ArrayList<>();

            pessoas.add("Pedro");
            pessoas.add("Joao");
            pessoas.add("Tiago");
            pessoas.add("Filipe");
            pessoas.add("Mateus");
            pessoas.add("Andre");
            pessoas.add("Simao");

            return pessoas.stream()
                    .filter( pessoa -> pessoa.length() == 5 )
                    .collect(Collectors.toList());
        };

        System.out.println("Supplier - nomes com cinco letras: " + nomesComCincoLetras.get());
    }

    /**
     *
     * Obs: Os tres pontos (chama-se vargargs) e significa que esta funcao recebe um numero variavel
     * de argumentos,todos os tipo String, que serao colocados, em tempo de execucao, num array
     * chamado 'nomes'.
     *
     * No geral, um parâmetro vararg pode receber 0, muitos ou um array de parâmetros.
     * Por isso, o parâmetro vararg deve ser o último parâmetro do método.
     *
     */
    public static void iteracoes(String... nomes)
    {
        String nomesAgrupadosComFor = "";

        for(int i=0; i < nomes.length; i++) {
            nomesAgrupadosComFor = nomesAgrupadosComFor.concat(nomes[i] + ", ");
        }
        System.out.println("Iteracao e agrupamento com For: " + nomesAgrupadosComFor);

        String nomesAgrupadosComStream = Stream.of(nomes).collect(Collectors.joining(", "));
        System.out.println("Iteracao e agrupamento com Stream: " + nomesAgrupadosComStream);

        List<String> nomesFiltradosComFor = new ArrayList<>();
        for(int i=0; i < nomes.length; i++) {
            if(nomes[i].length() == 5) {
                nomesFiltradosComFor.add(nomes[i]);
            }
        }
        System.out.println("Iteracao e filtragem com For: " + nomesFiltradosComFor);

        List nomesFiltradosComStream = Stream.of(nomes)
                .filter(nome -> nome.length() == 5)
                .collect(Collectors.toList());
        System.out.println("Iteracao e filtragem com Stream: " + nomesFiltradosComStream);
    }

}


/**
 Interfaces funcionais possuem apenas um metdoto abstrato.

 Geralmente, pois nao eh obrigatorio, possuem a anotacao em nivel de classe @FunctionalInterface,
 para forçcar o compilador a apresentar um erro se a interface nao estiver de acordo com as
 regras de uma  interface funcional.

 */
@FunctionalInterface
interface RetornaString {
    String gerar(String valor);
}

interface ConcatenaString {
    String concatena(String s1, String s2);
}

interface Calculo {
    public int calcular(int a, int b);
}