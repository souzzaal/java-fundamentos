package br.com.souzzaal.fundamentos.paradigmaFuncional;

import java.util.function.UnaryOperator;

/**

 Paradigma Imperativo: Expressa o codigo atraves de comandos ao computador, nele eh possivel
 ter controle de estados de objetos. Eh como se fosse um script de ordens que o computador
 vai executando uma apos a outra. Ver metodo imperativo.

 Paradigma Funcional: Damos uma regra, uma declaracao de como queremos que o programa se
 comporte. Ver metodo funcional.

 A programacao funcional eh o processo de construir software atraves da composicao de
 funcoes puras, evitando compartilhamento de estados, dados mutaveis e efeitos colaterais.
 Eh declarativa ao inves de imperativa.

 */

public class ImperativoVsFuncional {

    public static void main(String[] args) {

        imperativo();
        funcional();
    }

    private static void imperativo()
    {
        int valor = 10;
        int resultado = valor * 3;
        System.out.println("Imperativo: " + resultado);
    }

    private static void funcional()
    {
        UnaryOperator<Integer> lambdaCalculaValorVezesTres = valor -> valor*3;
        System.out.println("Funcional: " + lambdaCalculaValorVezesTres.apply(10));
    }

}
