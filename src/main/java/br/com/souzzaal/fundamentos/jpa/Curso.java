package br.com.souzzaal.fundamentos.jpa;

import javax.persistence.*;

@Entity
@Table(name = "cursos")
public class Curso {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private String nome;

	@Column(name = "duracao_horas")
	private Float duracaoHoras;

	public Curso(String nome, Float duracao) {
		this.nome = nome;
		this.duracaoHoras = duracao;
	}

	public Curso() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Float getDuracaoHoras() {
		return duracaoHoras;
	}

	public void setDuracaoHoras(Float duracao_horas) {
		this.duracaoHoras = duracao_horas;
	}

	@Override
	public String toString() {
		return "Curso{" + "id=" + id + ", nome='" + nome + '\'' + ", duracao_horas=" + duracaoHoras + '}';
	}
}
