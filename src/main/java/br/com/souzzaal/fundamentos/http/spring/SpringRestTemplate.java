package br.com.souzzaal.fundamentos.http.spring;

import org.springframework.http.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class SpringRestTemplate {

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();

        // GET
        getForObject(restTemplate);
        getForEntity(restTemplate);

        // POST
        postForObject(restTemplate);
        postForEntity(restTemplate);
        postForLocation(restTemplate);

        // PUT
        put(restTemplate);

        // DELETE
        delete(restTemplate);

        // HEADERS
        retrieveHeaders(restTemplate);

        // OPTIONS
        allowedOperations(restTemplate);

        // Qualquer metodo HTTP
        exchange(restTemplate);

        // Erros
        errors(restTemplate);

    }

    /**
     * Faz uma requisicao GET para a URL e converte o resultado no tipo especificado
     *
     * @param restTemplate
     */
    public static void getForObject(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts";

        String json = restTemplate.getForObject(url, String.class); // converte numa String
        System.out.println("====> getForObject - String: " + json + "\n\n");

        Post[] posts = restTemplate.getForObject(url, Post[].class); // converte num array de Post
        System.out.println("===> getForObject - Array: " + posts.length + "\n\n");


        Post post1 = restTemplate.getForObject(url + "/{id}", Post.class, 1); // converte numa String
        System.out.println("====> getForObject - Paramenters: " + post1.getUserId() + "\n\n");
    }

    /**
     * Permite manipular a resposta (statusCode, headers, body...)
     *
     * @param restTemplate
     */
    public static void getForEntity(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts/{id}";
        ResponseEntity<Post> response = restTemplate.getForEntity(url, Post.class, 1);
        System.out.println("====> getForEntity - StatusCode: " + response.getStatusCode() + "\n\n");
    }


    public static void postForObject(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts";

        var post = new Post();
        post.setTitle("title");
        post.setBody("body");
        post.setUserId(1);

        HttpEntity<Post> entity = new HttpEntity<>(post);

        String response = restTemplate.postForObject(url, entity, String.class); // converte numa String (poderia ser um Post)
        System.out.println("====> postForObject: " + response + "\n\n");
    }


    /**
     * Permite manipular a resposta
     * @param restTemplate
     */
    public static void postForEntity(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts";

        var post = new Post();
        post.setTitle("title");
        post.setBody("body");
        post.setUserId(1);

        HttpEntity<Post> entity = new HttpEntity<>(post);

        ResponseEntity<Post> response = restTemplate.postForEntity(url, entity, Post.class); // converte numa String
        System.out.println("====> postForEntity - Status " + response.getStatusCode() +  " postId: " + response.getBody().getId() + "\n\n");
    }

    /**
     * Retorna a URI ao inves de um objeto
     * @param restTemplate
     */
    public static void postForLocation(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts";

        var post = new Post();
        post.setTitle("title");
        post.setBody("body");
        post.setUserId(1);

        HttpEntity<Post> entity = new HttpEntity<>(post);

        URI response = restTemplate.postForLocation(url, entity, Post.class); // converte numa String
        System.out.println("====> postForLocation - URI " + response + "\n\n");
    }

    /**
     * Put nao retorna nada
     * @param restTemplate
     */
    public static void put(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts/{id}";

        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // create a post object
        Post post = new Post(4, "New Title", "New Body");

        // build the request
        HttpEntity<Post> entity = new HttpEntity<>(post, headers);

        // send PUT request to update post with id 10
        restTemplate.put(url, entity, 10);
        System.out.println("===> Put \n\n");
    }

    /**
     * Delete nao retorna nada
     * @param restTemplate
     */
    public static void delete(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts/{id}";

        restTemplate.delete(url, 10);
        System.out.println("===> Delete \n\n");
    }

    public static void retrieveHeaders(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts";

        // send HEAD request
        System.out.println("===> Headers" + restTemplate.headForHeaders(url) + "\n\n");
    }

    public static void allowedOperations(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts";

        // send HEAD request
        System.out.println("===> Options: " + restTemplate.optionsForAllow(url) + "\n\n");
    }

    /**
     * Permite criar uma requisicao customizada.
     * Exchange pode ser usado com qualquer metodo HTTP
     *
     * @param restTemplate
     */
    public static void exchange(RestTemplate restTemplate) {
        String url = "https://jsonplaceholder.typicode.com/posts/{id}";

        // create headers
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "asdf");

        // build the request
        HttpEntity request = new HttpEntity(headers);
        System.out.println("===> Exchange request headers: " + headers);

        // use `exchange` method for HTTP call
        ResponseEntity<Post> response = restTemplate.exchange(url, HttpMethod.GET, request, Post.class, 1);
        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println("===> Exchange:  " + response.getBody().getUserId() + "\n\n");
        } else {
            System.out.println("Erro - StatusCode: " + response.getStatusCode());
        }
    }

    public static void errors(RestTemplate restTemplate) {
        try {
            String url = "https://jsonplaceholder.typicode.com/404";
            restTemplate.getForObject(url, String.class);
        } catch (HttpStatusCodeException ex) {
            // raw http status code e.g `404`
            System.out.println(ex.getRawStatusCode());
            // http status code e.g. `404 NOT_FOUND`
            System.out.println(ex.getStatusCode().toString());
            // get response body
            System.out.println(ex.getResponseBodyAsString());
            // get http headers
            HttpHeaders headers= ex.getResponseHeaders();
            System.out.println(headers.get("Content-Type"));
            System.out.println(headers.get("Server"));
        }
    }



}
