package br.com.souzzaal.fundamentos.paradigmaFuncional;

import java.util.Arrays;
import java.util.function.BiPredicate;
import java.util.function.UnaryOperator;

/**

 Paradigma Imperativo: Expressa o codigo atraves de comandos ao computador, nele eh possivel
 ter controle de estados de objetos. Eh como se fosse um script de ordens que o computador
 vai executando uma apos a outra. Ver metodo imperativo.

 Paradigma Funcional: Damos uma regra, uma declaracao de como queremos que o programa se
 comporte. Ver metodo funcional.

 A programacao funcional eh o processo de construir software atraves da composicao de
 funcoes puras, evitando compartilhamento de estados, dados mutaveis e efeitos colaterais.
 Eh declarativa ao inves de imperativa.

 Conceitos fundamentais da programacao funcional

 - Composicao de funcoes: cria-se uma nova funcao atraves da composicao de outras.
 - Funcoes puras: quando invocada mais de uma vez produz exatamente o mesmo resultado.
 - Imutabilidade: uma vez que uma variavel recebeu um valor, este nao pode ser modificado.
 - Declarativo: declara o que se deseja, sem executa-lo imediatamente.

 */

public class ConceitosFundamentais {

    public static void main(String[] args) {

        imperativo();
        composicaoDeFuncoes();
        funcoesPuras();
        imutabilidade();
    }

    /**
     * Apenas para facilitar a compreensao.
     *
     * Cada instrucao eh uma ordem a ser executada no momento em que for avaliada.
     *
     * Ao avaliar "int valor = 10", a posicao de memoria eh reservada e a operaçcao de atribuicao eh realizada.
     * Ao avaliar "int resultado = valor * 3", a posicao de memoria eh reservada e a operaçcao de multiplicacao eh realizada.
     *
     */
    private static void imperativo()
    {
        int valor = 10;
        int resultado = valor * 3;
        System.out.println("Imperativo: " + resultado);
    }


    /**
     * Declarativo: declara o que se deseja, sem executa-lo imediatamente.
     *
     * Ao executar "UnaryOperator<Integer> lambdaCalculaValorVezesTres = valor -> valor*3;"
     * apenas informa-se que ao receber um valor ele deve ser multiplicado por tres, mas a
     * operacao em si nao eh realizada. Ou seja, o comportamento da funcao foi declarado.
     */
    private static void declarativo()
    {
        UnaryOperator<Integer> lambdaCalculaValorVezesTres = valor -> valor*3;
        System.out.println("Funcional: " + lambdaCalculaValorVezesTres.apply(10));
    }

    /**
     * Composicao de funcoes: cria-se uma nova funcao atraves da composicao de outras.
     *
     * Exemplo de como criar um algoritmo funcional que filtra os numeros pares de um
     * array e os multiplica por 2.
     */
    private static void composicaoDeFuncoes()
    {
        int[] numeros = {1,2,3,4};
        Arrays.stream(numeros)
                .filter(numero -> numero % 2 == 0)  // recebe uma funcao lambda como argumento
                .map(numero -> numero * 2) // recebe uma funcao lambda como argumento
                .forEach(numero -> System.out.println("Composicao de Funcoes: " + numero)); // recebe uma funcao lambda como argumento
    }

    /**
     * Funcoes puras: quando invocada mais de uma vez produz exatamente o mesmo resultado
     * para o mesmo parametro de entrada.
     *
     * Base de funcoes da matematica: f(x)
     *
     */
    private static void funcoesPuras()
    {
        BiPredicate<Integer, Integer> verificaSeEhMaior = (v1, v2) -> v1 > v2;

        System.out.println("Funcoes puras: 2 > 1? " + verificaSeEhMaior.test(2,1));
        System.out.println("Funcoes puras: 2 > 1? " + verificaSeEhMaior.test(2,1));
    }

    /**
     * Imutabilidade: uma vez que uma variavel recebeu um valor, este nao pode ser modificado.
     * As funcoes lambda usam variaveis com modificador final, o que impede que o valor seja modificado.
     */
    private static void imutabilidade()
    {
        int valor = 10;
        UnaryOperator<Integer> retornaDobro = v -> v * 2;
        System.out.println("Imutabilidade - variavel original: " + valor);
        System.out.println("Imutabilidade - aplicando dobro: " + retornaDobro.apply(valor));
        System.out.println("Imutabilidade - variavel original: " + valor);
    }


}
