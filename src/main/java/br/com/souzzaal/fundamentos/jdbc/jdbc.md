# JDBC

## O que e?

JDBC (Java Database Connectivity) é uma API com diversas classes e interfaces escritas na linguagem Java que estão presentes nos pacotes java.sql e javax.sql. Elas permitem que programas em Java realizem conexões em bancos de dados para realizar consultas. Uma dessas classes principais é o driver JDBC que intermedia essa interação.

Sem a API JDBC, seria necessário conhecer o protocolo proprietário de cada banco de dados para se conectar e realizar consultas. Já com a API JDBC, é utilizada somente UMA interface Java para qualquer banco de dados, deixando o driver implementar as especificações de cada banco de dados, enquanto o desenvolvedor se preocupa apenas em selecionar um driver e criar as queries (neste caso, consultas SQL).



É necessário adicionar no projeto driver JDBC para o bando de dados que aplicação realizará comunicação. Para este exemplo, utilizaremos um banco MySQL, e como estamos num projeto Spring com Gradle, adicionamos como dependências no arquivo `build.gradle`.

```textile
dependencies {
	compile group: 'org.springframework', name: 'spring-jdbc', version: '5.2.9.RELEASE'
	compile group: 'mysql', name: 'mysql-connector-java', version: '8.0.21'
}
```

## Classes e interfaces utilizadas

Para realizar a comunicação com o banco de dados será utilizada classe **DriverManager** e interface **Connection**:

- DriveManager
  
  - Responsável pela comunicação com os drivers disponíveis. 
  
  - É utilizada para criar uma Connection com o BD através de uma URL
    
    - A URL especifica o driver, a localização e nome do BD

- Connection
  
  - Representa a conexão com o banco de dados
  
  - Permite criar **Statements**, que constroem consultas SQL



## Consultas com JDBC

Existem 3 interfaces para montar comandos SQL:

- Statement
  
  - Executar SQL comuns

- PreparedStatement
  
  - Executar SQL parametrizáveis
  
  - São preferíveis ao Statement por:
    
    - Prevenir SQL Injection
    
    - Melhoria da legibilidade  e desempenho

- CallableStatement
  
  - Executar stored procedures



Para executar comandos SQL:

- execute
  
  -  Pode executar qualquer qualquer tipo de SQL.

- executeQuery
  
  - Usado para executar "SELECT"

- executeUpdate
  
  - Usados para comandos de alteração do banco
    
    - Ex: INSERT, UPDATE, DELETE, CREATE ALTER



Para manipular os dados retornados pelo banco:



- ResultSet
  
  - Objeto que contem os dados de uma consulta
    
    - normalmente SELECT
  
  - Utiliza-se getters para buscar dados:
    
    - getInt para dados Inteiros
    
    - getString para Strings
    
    - ...

O método next() é utilizado para percorrer os dados.



Exemplo simplista para ilustrar os conceitos apresentados.

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionJDBC {

    public static void main(String[] args) {

        // 1 - NÃO ESQUECER DE BAIXAR O DRIVER PARA O BANCO DE DADOS QUE IRÁ UTILIZAR! (Como demonstrado na parte 1 do curso)

        // 2 - Definir parâmetros para se conectar ao banco de dados MySQL.
        String driver = "mysql";
        String dataBaseAddress = "localhost";
        String dataBaseName = "dbname";
        String user = "root";
        String password = "password";

        // 3 - Construção da string de conexão.
        StringBuilder sb = new StringBuilder("jdbc:")
                .append(driver).append("://")
                .append(dataBaseAddress).append("/")
                .append(dataBaseName);

        String connectionUrl = sb.toString(); //sb.toString() == "jdbc:mysql://localhost/digital_innovation_one"

        //Carregar a classe específica de implementação do driver na memória da JVM. (A partir da versão JDBC 4 não é mais necessário!!!)
        //Class.forName("com.mysql.jdbc.Driver");

        // 4 - Criar conexão usando o DriverManager, passando como parâmetros a string de conexão, usuário e senha do usuário.
        try (Connection conn = DriverManager.getConnection(connectionUrl, user, password)) {
            System.out.println("SUCESSO ao se conectar ao banco MySQL!");
            
            //Preparar consulta SQL.
            String sql = "SELECT * FROM aluno";

            //Preparar statement com os parâmetros recebidos (nesta função não tem parâmetros, pois irá retornar todos os valores da tabela aluno)
            PreparedStatement stmt = conn.prepareStatement(sql);

            //Executa consulta e armazena o retorno da consulta no objeto "rs".
            ResultSet rs = stmt.executeQuery();

            //Listagem dos alunos.
            while(rs.next()){
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                int idade = rs.getInt("idade");
                String estado = rs.getString("estado");
              
                System.out.println("Aluno: " + nome + " Idade: " + idade + " Estado: " + estado);
            }

        } catch (SQLException e) {
            System.out.println("FALHA ao se conectar ao banco MySQL!");
            e.printStackTrace();
        }

    }
}
```

## Referências

Texto e código baseados no trabalho de Daniel Karam, disponibilizado em https://github.com/danielkv7/digital-innovation-one/tree/master/Aula_JDBC_basico
