package br.com.souzzaal.fundamentos.collections.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * ArrayList asemelha-se aos arrays, contudo:
 *
 *   - nao tem limitacao de tamanho
 *   - garante ordem de insercao
 *   - permite adicao: atualizacao, leitra e remocao de forma simplificada
 *   - permite ordenacao
 *
 */
public class ArrayLists {

    public static void main(String[] args) {

        System.out.println("=== Manipulando ArrayList ===");

        List<String> nomes = new ArrayList<>();

        // Adicao de elementos
        nomes.add("Uva");
        nomes.add("Maça");
        nomes.add("Laranja");
        nomes.add("Abacate");
        nomes.add("Melao");
        System.out.println("Adicionado ao ArrayList os seguintes elementos: " + nomes);

        nomes.addAll(nomes);

        // Obtencao de elemento
        String elemento = nomes.get(1);
        System.out.println("Objeto da posicao 1: " + elemento );


        // Checagem de existencia
        boolean contemLaranja = nomes.contains("Laranja");
        System.out.println("Existe Laranja na lista: " + contemLaranja);


        // Edicao de elementos
        nomes.set(4, "Pera");
        System.out.println("Substituindo a posicao 4 por Pera: " + nomes);


        // Exclucao de elementos
        nomes.remove(4);
        System.out.println("Exclusao o indice 4: " + nomes);

        nomes.remove("Maça");
        System.out.println("Exclusao o objeto Maça: " + nomes);


        // Ordenacao de elementos
        Collections.sort(nomes);
        System.out.println("ArrayList ordenado por nome: " + nomes);


        // Tamanho da lista
        int tamanho = nomes.size();
        System.out.println("Quantidade de elementos na lista: " + tamanho);

        // Iterando sobre a collection
        for (String item : nomes) {
            System.out.println("Iteracao com for: " + item);
        }

        Iterator<String> iterator = nomes.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iteracao com while/iterator: " + iterator.next());
        }

        // Limpa a lista
        nomes.clear();
        System.out.println("Lista limpa: " + nomes);


        // Lista vazia
        boolean listaVazia = nomes.isEmpty();
        System.out.println("Lista vazia: " + listaVazia);

    }

}
